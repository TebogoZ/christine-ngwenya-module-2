
import 'dart:core';

class App {
   String app_name = '' ;
   String category = '' ;
   String developer = '' ;
   int year = 0;
  
  App ( String app_name,
      String category,
      String developer,
      int year,)
  {
         this.app_name = app_name;
         this.category = category;
         this.developer = developer;
         this.year = year;
  } 

void printInfo()
     {  
print('In ${this.year}, ${(this.app_name.toUpperCase())} won the MTN Business of the Year Award under ${this.category} . The app was developed by ${this.developer}.');
     }
    
     }
          
void main (){
      var app = App ("Standard Bank Shyft", "Best Financial Solution", "Arno von Helden", 2017);
      
      app.printInfo();
     }
